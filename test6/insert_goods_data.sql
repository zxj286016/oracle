DECLARE
    v_goods_id NUMBER;
    v_goods_name VARCHAR2(100);
    v_price NUMBER;
    v_production_date DATE;
BEGIN
    FOR i IN 1..100000 LOOP
        -- 生成商品数据
        v_goods_id := i;
        v_goods_name := '商品' || i;
        v_price := ROUND(DBMS_RANDOM.VALUE(1, 1000), 2);
        v_production_date := TRUNC(SYSDATE) - DBMS_RANDOM.VALUE(0, 365);

        -- 插入商品数据
        INSERT INTO GOODS (GOODS_ID, GOODS_NAME, PRICE, PRODUCTION_DATE)
        VALUES (v_goods_id, v_goods_name, v_price, v_production_date);
    END LOOP;
    
    COMMIT;
END;
/
