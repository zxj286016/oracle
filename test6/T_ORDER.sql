-- 创建订单表
CREATE TABLE T_ORDER (
  ID           NUMBER(10) PRIMARY KEY,
  USER_ID      NUMBER(10),
  ORDER_TIME   DATE,
  TOTAL_PRICE  NUMBER(12,2)
);