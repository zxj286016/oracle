# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 | [返回](./README.md)

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|

## 整体设计


字段名	数据类型	长度	说明
GOODS_ID	NUMBER(10)		商品ID，主键
GOODS_NAME	VARCHAR2(50)		商品名称
GOODS_PRICE	NUMBER(12,2)		商品单价
GOODS_STOCK	NUMBER(10)		商品库存
订单表 T_ORDER

字段名	数据类型	长度	说明
ORDER_ID	NUMBER(10)		订单ID，主键
USER_ID	NUMBER(10)		用户ID，外键
ORDER_TIME	DATE		下单时间
TOTAL_PRICE	NUMBER(12,2)		订单总价
用户表 T_USER

字段名	数据类型	长度	说明
USER_ID	NUMBER(10)		用户ID，主键
ACCOUNT	VARCHAR2(50)		用户账号
PASSWORD	VARCHAR2(50)		用户密码
订单明细表 T_ORDER_DETAIL

字段名	数据类型	长度	说明
DETAIL_ID	NUMBER(10)		明细ID，主键
ORDER_ID	NUMBER(10)		订单ID，外键
GOODS_ID	NUMBER(10)		商品ID，外键
GOODS_NAME	VARCHAR2(50)		商品名称
GOODS_PRICE	NUMBER(12,2)		商品单价
GOODS_COUNT	NUMBER(10)		商品数量

权限分配：
本商品销售系统共分配了两个用户，分别为sale_user和admin。其中， sale_user拥有商品表和用户表的读写权限，admin拥有订单表和订单明细表的读写权限。

表空间：
表空间1：存放商品信息表和订单详情表；
表空间2：存放订单表和用户表。

CREATE TABLESPACE SYSTEM
DATAFILE 'system01.dbf' SIZE 100M AUTOEXTEND ON;
CREATE TABLESPACE USER_DATA
DATAFILE 'user_data01.dbf' SIZE 500M AUTOEXTEND ON;
GRANT SELECT, INSERT, UPDATE, DELETE ON PRODUCT TO SALES_USER;
GRANT SELECT, INSERT, UPDATE, DELETE ON PRODUCT, STOCK TO STOCK_USER;


为了保证数据库的安全性，我们可以采用以下措施：

限制用户的访问权限，只允许用户操作其所需的表和数据；
对用户密码进行加密存储，避免敏感信息泄露；
对数据库备份文件进行加密存储。


## 实验步骤
### 1. 创建表空间和用户
表和表空间创建
```
CREATE TABLESPACE SYSTEM
DATAFILE 'system01.dbf' SIZE 100M AUTOEXTEND ON;

CREATE TABLESPACE USER_DATA
DATAFILE 'user_data01.dbf' SIZE 500M AUTOEXTEND ON;

GRANT SELECT, INSERT, UPDATE, DELETE ON PRODUCT TO SALES_USER;
GRANT SELECT, INSERT, UPDATE, DELETE ON PRODUCT, STOCK TO STOCK_USER;
```
### 2. 创建表
```
商品表：
CREATE TABLE products (
product_id NUMBER(10) PRIMARY KEY,
product_name VARCHAR2(50),
price NUMBER(10,2),
category VARCHAR2(50),
supplier VARCHAR2(50)
) TABLESPACE tbs_prod_index;

订单表：
CREATE TABLE orders (
order_id NUMBER(10) PRIMARY KEY,
customer_id NUMBER(10),
order_date DATE,
total_price NUMBER(10,2)
) TABLESPACE tbs_orders_index;

订单详情表:
CREATE TABLE order_details (
order_id NUMBER(10),
product_id NUMBER(10),
quantity NUMBER(10),
price NUMBER(10,2),
CONSTRAINT order_details_primary_key PRIMARY KEY (order_id, product_id)
) TABLESPACE tbs_order_detail_index;

用户表：
CREATE TABLE customers (
customer_id NUMBER(10) PRIMARY KEY,
customer_name VARCHAR2(50),
email VARCHAR2(50),
phone_number VARCHAR2(20),
address VARCHAR2(100)
) TABLESPACE tbs_cust_index;

库存表：
CREATE TABLE inventory (
product_id NUMBER(10) PRIMARY KEY,
stock_quantity NUMBER(10)
) TABLESPACE tbs_prod_index;
```
### 3.插入十万条数据
#### 3.1 批量插入商品信息数据
```
DECLARE
    TYPE goods_record IS RECORD (
        goods_id NUMBER(10),
        goods_name VARCHAR2(50),
        price NUMBER(8, 2),
        description VARCHAR2(2000)
    );
    
    TYPE goods_table IS TABLE OF goods_record;
    
    l_goods goods_table := goods_table();
BEGIN
    FOR i IN 1 .. 100000 LOOP
        l_goods.extend;
        l_goods(i).goods_id := i;
        l_goods(i).goods_name := '商品' || TO_CHAR(i);
        l_goods(i).price := ROUND(DBMS_RANDOM.VALUE(100, 10000), 2);
        l_goods(i).description := '商品' || TO_CHAR(i) || '的详细描述';
    END LOOP;
    
    FORALL i IN INDICES OF l_goods
        INSERT INTO goods(goods_id, goods_name, price, description)
        VALUES l_goods(i);
        
    COMMIT;
    DBMS_OUTPUT.PUT_LINE('商品信息插入成功！');
EXCEPTION
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('插入商品信息时出错：' || SQLERRM);
END;
```
使用 PL/SQL 实现批量插入商品信息表的数据，其中使用了随机数生成函数和 FORALL 语句，以提高效率。
#### 3.2：批量插入销售记录数据
```
DECLARE
    TYPE sale_record IS RECORD (
        sale_id NUMBER(10),
        customer_id NUMBER(10),
        goods_id NUMBER(10),
        amount NUMBER(8, 2),
        sale_time DATE
    );
    
    TYPE sale_table IS TABLE OF sale_record;
    
    l_sales sale_table := sale_table();
BEGIN
    FOR i IN 1 .. 100000 LOOP
        l_sales.extend;
        l_sales(i).sale_id := i;
        l_sales(i).customer_id := ROUND(DBMS_RANDOM.VALUE(1, 10000));
        l_sales(i).goods_id := ROUND(DBMS_RANDOM.VALUE(1, 100000));
        l_sales(i).amount := ROUND(DBMS_RANDOM.VALUE(1, 10), 2);
        l_sales(i).sale_time := SYSDATE - (DBMS_RANDOM.VALUE(1, 365) + DBMS_RANDOM.VALUE);
    END LOOP;
    
    FORALL i IN INDICES OF l_sales
        INSERT INTO sale_record(sale_id, customer_id, goods_id, amount, sale_time)
        VALUES l_sales(i);
        
    COMMIT;
    DBMS_OUTPUT.PUT_LINE('销售记录信息插入成功！');
EXCEPTION
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('插入销售记录信息时出错：' || SQLERRM);
END;
```
### 3.3 批量插入客户信息数据
```
DECLARE
    TYPE customer_record IS RECORD (
        customer_id NUMBER(10),
        customer_name VARCHAR2(50),
        gender VARCHAR2(6),
        age NUMBER(3),
        address VARCHAR2(100),
        phone VARCHAR2(20)
    );
    
    TYPE customer_table IS TABLE OF customer_record;
    
    l_customers customer_table := customer_table();
BEGIN
    FOR i IN 1 .. 100000 LOOP
        l_customers.extend;
        l_customers(i).customer_id := i;
        l_customers(i).customer_name := '客户' || TO_CHAR(i);
        l_customers(i).gender := CASE WHEN MOD(i, 2) = 1 THEN 'Male' ELSE 'Female' END;
        l_customers(i).age := ROUND(DBMS_RANDOM.VALUE(18, 80));
        l_customers(i).address := '客户' || TO_CHAR(i) || '的地址信息';
        l_customers(i).phone := '138' || LPAD(TRUNC(DBMS_RANDOM.VALUE(0, 100000000)), 8, '0');
    END LOOP;
    
    FORALL i IN INDICES OF l_customers
        INSERT INTO customer(customer_id, customer_name, gender, age, address, phone)
        VALUES l_customers(i);
        
    COMMIT;
    DBMS_OUTPUT.PUT_LINE('客户信息插入成功！');
EXCEPTION
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('插入客户信息时出错：' || SQLERRM);
END;
```
### 3.4 批量插入库存信息数据
```
DECLARE
    TYPE inventory_record IS RECORD (
        goods_id NUMBER(10),
        stock NUMBER(10)
    );
    
    TYPE inventory_table IS TABLE OF inventory_record;
    
    l_inventory inventory_table := inventory_table();
BEGIN
    FOR i IN 1 .. 100000 LOOP
        l_inventory.extend;
        l_inventory(i).goods_id := i;
        l_inventory(i).stock := ROUND(DBMS_RANDOM.VALUE(10, 1000));
    END LOOP;
    
    FORALL i IN INDICES OF l_inventory
        INSERT INTO inventory(goods_id, stock)
        VALUES l_inventory(i);
        
    COMMIT;
    DBMS_OUTPUT.PUT_LINE('库存信息插入成功！');
EXCEPTION
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('插入库存信息时出错：' || SQLERRM);
END;
```

### 4.创建包，并在包里面设计一些存储过程和函数
我们需要创建一个名为 "sales_management" 的 PL/SQL 包，该包将用于管理销售相关的数据和业务逻辑。代码：
```
CREATE OR REPLACE PACKAGE sales_management AS
    -- 存储过程：添加新商品
    PROCEDURE add_goods(
        goods_name IN VARCHAR2,
        price IN NUMBER,
        description IN VARCHAR2
    );
    
    -- 存储过程：更新库存信息
    PROCEDURE update_inventory(
        goods_id IN NUMBER,
        new_stock IN NUMBER
    );
    
    -- 函数：查询指定日期的销售记录数量
    FUNCTION count_sales_records(
        sale_date IN DATE
    ) RETURN NUMBER;
    
    -- 函数：查询指定商品 ID 的库存信息
    FUNCTION query_inventory(
        goods_id IN NUMBER
    ) RETURN NUMBER;
    
END sales_management;
/
```
在上述代码中，我们定义了一个名为 "sales_management" 的包，并在其中定义了一些存储过程和函数。其中，add_goods 存储过程用于添加新商品，update_inventory 存储过程用于更新库存信息，count_sales_records 函数用于查询指定日期的销售记录数量，query_inventory 函数用于查询指定商品 ID 的库存信息。

接下来，我们需要在包体中实现上述存储过程和函数的具体实现。下方代码，用于实现 add_goods 存储过程：
```
CREATE OR REPLACE PACKAGE BODY sales_management AS
    -- 存储过程：添加新商品
    PROCEDURE add_goods(
        goods_name IN VARCHAR2,
        price IN NUMBER,
        description IN VARCHAR2
    ) IS
    BEGIN
        INSERT INTO goods (goods_name, price, description)
        VALUES (goods_name, price, description);
    END add_goods;
    
    -- 存储过程：更新库存信息
    PROCEDURE update_inventory(
        goods_id IN NUMBER,
        new_stock IN NUMBER
    ) IS
    BEGIN
        UPDATE inventory
        SET stock = new_stock
        WHERE goods_id = goods_id;
    END update_inventory;
    
    -- 函数：查询指定日期的销售记录数量
    FUNCTION count_sales_records(
        sale_date IN DATE
    ) RETURN NUMBER IS
        cnt NUMBER;
    BEGIN
        SELECT COUNT(*) INTO cnt
        FROM sales
        WHERE TRUNC(sale_date) = TRUNC(sale_date);
        
        RETURN cnt;
    END count_sales_records;
    
    -- 函数：查询指定商品 ID 的库存信息
    FUNCTION query_inventory(
        goods_id IN NUMBER
    ) RETURN NUMBER IS
        stock NUMBER;
    BEGIN
        SELECT stock INTO stock
        FROM inventory
        WHERE goods_id = goods_id;
        
        RETURN stock;
    END query_inventory;
END sales_management;
/
```
在上述代码中，我们实现了 add_goods 存储过程、update_inventory 存储过程、count_sales_records 函数和 query_inventory 函数的具体功能。这些存储过程和函数可以根据需要进行修改和扩展，以满足更复杂的业务需求。

### 5.测试
测试 库存更新 
```
DECLARE
    goods_id NUMBER(10) := 123;
    new_stock NUMBER(10) := 50;
BEGIN
    update_inventory(goods_id, new_stock);
    DBMS_OUTPUT.PUT_LINE('库存信息更新成功！');
EXCEPTION
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('更新库存信息出错：' || SQLERRM);
END;

```
查询库存信息
```
DECLARE
    goods_id NUMBER(10) := 123;
    stock NUMBER(10);
BEGIN
    stock := query_inventory(goods_id);
    DBMS_OUTPUT.PUT_LINE('商品 ' || goods_id || ' 的库存为 ' || stock);
EXCEPTION
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('查询库存信息出错：' || SQLERRM);
END;

```
测试  查询指定日期的销售记录数量
DECLARE
    sale_date DATE := TO_DATE('2023-05-24', 'YYYY-MM-DD');
    record_cnt NUMBER(10);
BEGIN
    record_cnt := sales_management.count_sales_records(sale_date);
    DBMS_OUTPUT.PUT_LINE('销售记录数量为：' || record_cnt);
EXCEPTION
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('查询销售记录数量出错：' || SQLERRM);
END;
测试  查询指定商品 ID 的库存信息
DECLARE
    goods_id NUMBER(10) := 123;
    stock NUMBER(10);
BEGIN
    stock := sales_management.query_inventory(goods_id);
    DBMS_OUTPUT.PUT_LINE('商品 ' || goods_id || ' 的库存为 ' || stock);
EXCEPTION
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('查询库存信息出错：' || SQLERRM);
END;

测试 查询一天总营业额
```
DECLARE
    sale_date DATE := TO_DATE('2023-05-24', 'YYYY-MM-DD');
    total_sales NUMBER(12, 2);
BEGIN
    total_sales := query_daily_sales(sale_date);
    DBMS_OUTPUT.PUT_LINE('指定日期（' || TO_CHAR(sale_date, 'YYYY-MM-DD') || '）的总营业额为：' || TO_CHAR(total_sales, '$999,999,999.99'));
EXCEPTION
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('查询一天总营业额出错：' || SQLERRM);
END;

```
测试 计算日均营业额
```
DECLARE
    avg_sales NUMBER(12, 2);
BEGIN
    avg_sales := calc_avg_sales();
    DBMS_OUTPUT.PUT_LINE('日均营业额为：' || TO_CHAR(avg_sales, '$999,999,999.99'));
EXCEPTION
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('计算日均营业额出错：' || SQLERRM);
END;


```
### 6.存储过程和函数
添加新商品
```
CREATE OR REPLACE PROCEDURE add_goods(
    goods_name IN VARCHAR2,
    price IN NUMBER,
    description IN VARCHAR2)
IS
    new_id NUMBER(10);
BEGIN
    SELECT MAX(goods_id) + 1 INTO new_id FROM goods;
    INSERT INTO goods (goods_id, goods_name, price, description)
    VALUES (new_id, goods_name, price, description);
    INSERT INTO inventory (goods_id, stock) VALUES (new_id, 0);
    COMMIT;
END add_goods;
```
该存储过程接收三个参数，根据当前最大的商品 ID 自动生成新的商品 ID，并向 goods 表中插入新数据，并在 inventory 表中添加一条新记录。


删除商品
```
CREATE OR REPLACE PROCEDURE delete_goods(
    goods_id IN NUMBER)
IS
BEGIN
    DELETE FROM goods WHERE goods_id = goods_id;
    DELETE FROM inventory WHERE goods_id = goods_id;
    COMMIT;
END delete_goods;
```
查询客户购买情况
```
CREATE OR REPLACE FUNCTION query_sales_amount(
    customer_id IN NUMBER,
    start_time IN DATE,
    end_time IN DATE)
RETURN NUMBER
IS
    total_amount NUMBER(12, 2) := 0;
BEGIN
    FOR i IN (SELECT * FROM sale_record WHERE customer_id = customer_id AND sale_time >= start_time AND sale_time <= end_time) LOOP
        total_amount := total_amount + i.amount * (SELECT price FROM goods WHERE goods_id = i.goods_id);
    END LOOP;
    RETURN total_amount;
END query_sales_amount;
```
该函数接收三个参数，查询指定客户在指定时间段内的购买记录，并计算出总金额。
### 7.备份方案
在该系统中，我们将采用以下备份方案：

每天晚上定期执行完整备份，保存一周内的备份文件，使用 RMAN 工具进行备份。
每小时执行一次差异备份，保存一天内的备份文件，使用 RMAN 工具进行备份。
每15分钟备份一次归档日志，保存三天内的备份文件，使用 RMAN 工具进行备份。

代码：
# 完整备份脚本 full_backup.sh
#!/bin/sh
rman target / <<EOF
run {
    allocate channel ch1 type disk;
    backup as compressed backupset database format '/u01/backup/full_%d_%s_%p_%T.bkp';
    release channel ch1;
    delete noprompt obsolete recovery window of 7 days;
}
exit;

# 差异备份脚本 incremental_backup.sh
#!/bin/sh
rman target / <<EOF
run {
    allocate channel ch1 type disk;
    backup as compressed backupset incremental level 1 format '/u01/backup/incr_%d_%s_%p_%T.bkp' (database);
    release channel ch1;
    delete noprompt obsolete recovery window of 1 days;
}
exit;

# 归档日志备份脚本 archive_backup.sh
#!/bin/sh
rman target / <<EOF
run {
    allocate channel ch1 type disk;
    backup as compressed backupset archivelog all format '/u01/backup/arch_%d_%s_%p_%T.bkp' delete input;
    release channel ch1;
    delete noprompt obsolete recovery window of 3 days;
}
exit;

0 0 * * * /bin/bash -l -c '/u01/backup/full_backup.sh'  # 每天晚上 0:00 执行完整备份
0 * * * * /bin/bash -l -c '/u01/backup/incremental_backup.sh'  # 每小时执行差异备份
*/15 * * * * /bin/bash -l -c '/u01/backup/archive_backup.sh'  # 每 15 分钟执行归档日志备份


## 总结
PL/SQL 是一种过程化编程语言，它是 Oracle 数据库中的一部分，它扩展了 SQL 语言，使得我们可以在数据库中编写出更为复杂的处理逻辑，并且可以使用 Oracle 提供的大量的系统对象和函数。

本次实验中，我们就使用了 PL/SQL 来生成数据。通过 PL/SQL 中的循环结构和随机数生成函数，我们可以轻松地生成出大量的样本数据。同时，由于 PL/SQL 可以直接操作数据库对象，因此我们可以直接把数据插入到表中，生成完整的测试数据集。

数据库设计
在这个实验中，我们需要生成四张表的数据：商品信息、销售记录、客户信息和库存信息。这四张表之间存在着关联关系，必须要按照一定的顺序来插入数据，否则会出现错误。

在设计数据库时，我们需要考虑表之间的关系以及字段的类型和长度等问题。另外，在插入数据时，我们还需要确保唯一性约束不被破坏，否则会导致插入数据失败。

数据库性能
在生成大量的数据时，数据库性能会成为一个非常重要的问题。如果我们的程序写得不好，或者数据库服务器不够强大，那么生成数据的速度就会非常慢。

为了提高数据库性能，我们需要考虑以下几个因素：

合理设计表结构，避免冗余字段；
使用正确的数据类型和长度；
减少批量插入的次数，一次性插入尽可能多的数据；
开启自动提交，并且在合适的时候手动提交事务。
数据生成效果
最后，我们需要评估生成的测试数据是否达到了预期的目标。通过检查生成的数据是否符合规范，以及使用测试用例模拟实际场景中的数据，可以来评估数据生成的效果。

如果生成的数据质量较高，那么我们就可以在开发、测试和生产环境中使用这些数据了。如果生成的数据质量不尽如人意，那么我们就需要重新审视程序的逻辑，改进代码，优化数据库结构，从而生成出更为优秀的测试数据集。

总之，本次实验让我更加熟练了解了 PL/SQL 编程语言


