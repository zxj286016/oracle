DECLARE
    v_goods_id NUMBER;
    v_price NUMBER;
    v_sale_id NUMBER;
    v_sale_time DATE;
    v_quantity NUMBER;
    v_amount NUMBER;
BEGIN
    FOR i IN 1..100000 LOOP
        -- 获取商品数据
        SELECT PRICE INTO v_price FROM GOODS WHERE GOODS_ID = i;

        -- 生成销售记录数据
        v_sale_id := i;
        v_sale_time := SYSDATE - DBMS_RANDOM.VALUE(0, 365);
        v_quantity := ROUND(DBMS_RANDOM.VALUE(1, 10));
        v_amount := v_quantity * v_price;

        -- 插入销售记录数据
        INSERT INTO SALE_RECORD (SALE_ID, SALE_TIME, QUANTITY, AMOUNT)
        VALUES (v_sale_id, v_sale_time, v_quantity, v_amount);
    END LOOP;
    
    COMMIT;
END;
/
