-- 张三下单 iPhone 12，购买 2 台
BEGIN
  T_ORDER_PKG.INSERT_ORDER(1, 1, 2);
END;
/

-- 李四下单 MacBook Pro，购买 1 台
BEGIN
  T_ORDER_PKG.INSERT_ORDER(2, 3, 1);
END;
/

-- 张三下单 AirPods Max，购买 3 台
BEGIN
  T_ORDER_PKG.INSERT_ORDER(1, 4, 3);
END;
/

-- 王五下单 iPad Air，购买 2 台
BEGIN
  T_ORDER_PKG.INSERT_ORDER(3, 2, 2);
END;
/

-- 张三下单 Apple Watch SE，购买 5 台
BEGIN
  T_ORDER_PKG.INSERT_ORDER(1, 5, 5);
END;
/