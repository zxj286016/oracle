CREATE OR REPLACE PACKAGE BODY T_ORDER_PKG IS
  PROCEDURE INSERT_ORDER(
    p_user_id      IN T_USER.ID%TYPE,
    p_goods_id     IN T_GOODS.GOODS_ID%TYPE,
    p_goods_count  IN T_ORDER_DETAIL.GOODS_COUNT%TYPE
  ) IS
    v_total_price  T_ORDER.TOTAL_PRICE%TYPE;
    v_order_id     T_ORDER.ID%TYPE;
    v_detail_id    T_ORDER_DETAIL.ID%TYPE;
  BEGIN
    -- 创建新订单
    INSERT INTO T_ORDER (USER_ID) VALUES (p_user_id) RETURNING ID INTO v_order_id;

    -- 创建订单明细
    SELECT GOODS_NAME, GOODS_PRICE INTO v_detail_id FROM T_GOODS WHERE GOODS_ID = p_goods_id;
    INSERT INTO T_ORDER_DETAIL (ORDER_ID, GOODS_ID, GOODS_NAME, GOODS_PRICE, GOODS_COUNT) VALUES (v_order_id, p_goods_id, v_detail_id.GOODS_NAME, v_detail_id.GOODS_PRICE, p_goods_count);

    -- 更新订单总价
    SELECT SUM(GOODS_PRICE * GOODS_COUNT) INTO v_total_price FROM T_ORDER_DETAIL WHERE ORDER_ID = v_order_id;
    UPDATE T_ORDER SET TOTAL_PRICE = v_total_price WHERE ID = v_order_id;
  END INSERT_ORDER;
END T_ORDER_PKG;
