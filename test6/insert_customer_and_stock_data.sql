DECLARE
    v_goods_id NUMBER;
    v_price NUMBER;
    v_quantity NUMBER;
    v_amount NUMBER;
    v_customer_id NUMBER;
    v_customer_name VARCHAR2(100);
    v_phone VARCHAR2(20);
BEGIN
    FOR i IN 1..100000 LOOP
        -- 获取商品数据
        SELECT GOODS_ID, PRICE INTO v_goods_id, v_price FROM GOODS WHERE GOODS_ID = i;

        -- 生成客户数据
        v_customer_id := i;
        v_customer_name := '客户' || i;
        v_phone := '手机号' || i;

        -- 插入客户数据
        INSERT INTO CUSTOMER (CUSTOMER_ID, CUSTOMER_NAME, PHONE)
        VALUES (v_customer_id, v_customer_name, v_phone);

        -- 生成库存数据
        v_quantity := ROUND(DBMS_RANDOM.VALUE(10, 100));
        v_amount := v_quantity * v_price;

        -- 插入库存数据
        INSERT INTO STOCK (GOODS_ID, QUANTITY, AMOUNT)
        VALUES (v_goods_id, v_quantity, v_amount);
    END LOOP;
    
    COMMIT;
END;
/
