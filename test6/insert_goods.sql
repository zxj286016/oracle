INSERT INTO T_GOODS (GOODS_NAME, GOODS_PRICE, GOODS_STOCK)
VALUES ('iPhone 12', 8999, 100),
       ('iPad Air', 5999, 50),
       ('MacBook Pro', 13999, 20),
       ('AirPods Max', 5499, 30),
       ('Apple Watch SE', 2999, 80);
