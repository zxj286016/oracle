## 实验4：PL/SQL语言打印杨辉三角
### 实验目的
掌握Oracle PL/SQL语言以及存储过程的编写。
### 实验内容
1. 杨辉三角源代码。

```
set serveroutput on;
declare
type t_number is varray (100) of integer not null; --数组
i integer;
j integer;
spaces varchar2(30) :='   '; --三个空格，用于打印时分隔数字
N integer := 9; -- 一共打印9行数字
rowArray t_number := t_number();
begin
    dbms_output.put_line('1'); --先打印第1行
    dbms_output.put(rpad(1,9,' '));--先打印第2行
    dbms_output.put(rpad(1,9,' '));--打印第一个1
    dbms_output.put_line(''); --打印换行
    --初始化数组数据
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N --打印每行，从第3行起
    loop
        rowArray(i):=1;    
        j:=i-1;
            --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
            --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j),9,' '));--打印第一个1
        end loop;
        dbms_output.put_line(''); --打印换行
    end loop;
END;
/

```
2. 将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriange，存储起来。存储过程接受行数N作为参数。

- 2.1 代码完成
```
create or replace procedure yhtriange (n in integer) is
  type t_number is varray(100) of integer not null; --数组
  i integer;
  j integer;
  spaces varchar2(30) := '   ';--三个空格，用于打印时分隔数字
  rowarray t_number := t_number();
begin
  dbms_output.put_line('1'); --先打印第1行
  dbms_output.put(rpad(1, 9, ' ')); --先打印第2行
  dbms_output.put(rpad(1, 9, ' ')); --打印第一个1
  dbms_output.put_line(''); --打印换行

  --初始化数组数据
  for i in 1 .. n loop
    rowarray.extend;
  end loop;
  
  rowarray(1) := 1;
  rowarray(2) := 1;

  for i in 3 .. n --打印每行，从第3行起
  loop
    rowarray(i) := 1;
    j := i - 1;
    
        --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
        --这里从第j-1个数字循环到第2个数字，顺序是从右到左
    while j > 1 loop
      rowarray(j) := rowarray(j) + rowarray(j - 1);
      j := j - 1;
    end loop;
    
    --打印第i行
    for j in 1 .. i loop
      dbms_output.put(rpad(rowarray(j), 9, ' ')); --打印第一个1
    end loop;
  
    dbms_output.put_line(''); --打印换行
  end loop;

end;
/
- 2.2 工具完成

步骤：打开oracle sql Developer，找到hr表，往下翻到过程，右键新建过程，修改过程名字，添加过程参数。在begin end里面添加杨辉三角begin end的代码
3. 运行这个存储过程即可以打印出N行杨辉三角。
``` 
exec yhtriange(9);

exec yhtriange(20);
### 总结
在实验中，我们定义了一个类型为 varray 的数组类型 t_number，并定义了变量 i、j 和 spaces。然后，我们初始化了第一和第二行的数据，并进行了循环计算并打印输出了每一行的数据。

在转为存储过程时，我们添加了一个 IN 类型的参数 n，用于指定打印的行数。然后，我们将原来的代码放进存储过程体内，并修改了循环的终止条件为 n。

最后，我们可以通过调用该存储过程，传入不同的行数参数，从而打印出对应行数的杨辉三角。

本次实验让我更好地了解了 PL/SQL 语言的基础语法和存储过程的使用
